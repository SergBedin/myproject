
var data = {
  h: 0,
  m: 0,
  s: 0,
  ha: "",
  ma: "",
  daynumber: 1,
  beginstart: "1s",
};

function startTime() {
  var today=new Date();
  var h=today.getHours();
  var m=today.getMinutes();
  var s=today.getSeconds();
  var month = (today.getMonth()+1)*30 + 'deg';
  var weekday = today.getDay()*51.43 + 'deg';
  if (today.getDate() < 10) {
    var daynumber = "0" + today.getDate();
  }
  else {
    var daynumber = today.getDate();
  }
  data.h = h;
  data.m = m;
  data.s = s;
  data.daynumber = daynumber;
  var hour = (30 * h + 0.5 * m) + 'deg';
  var min = (6 * m + 0.1 * s) + 'deg';
  var sec = (6 * s) + 'deg';
  document.documentElement.style.setProperty('--secund', sec);
  document.documentElement.style.setProperty('--minut', min);
  document.documentElement.style.setProperty('--hour', hour);
  document.documentElement.style.setProperty('--month', month);
  document.documentElement.style.setProperty('--weekday', weekday);
  t=setTimeout('startTime()',500);
  return daynumber
};
function animereset() {
  document.documentElement.style.setProperty('--visiblogo', "none");
  document.documentElement.style.setProperty('--lightplanet', "none");
  document.documentElement.style.setProperty('--rocket', "none");
  document.documentElement.style.setProperty('--flame', "none");
  document.documentElement.style.setProperty('--sun', "none");
}

startTime();

/////////////////////////////////////////////////////////////////////////
var vm = new Vue ({
  el: '#clockshop',
  data: data,
  methods: {
    animation: function() {
      document.documentElement.style.setProperty("--visiblogo", "visiblogo");
      document.documentElement.style.setProperty("--lightplanet", "lightplanet");
      document.documentElement.style.setProperty("--rocket", "rocket");
      document.documentElement.style.setProperty("--flame", "flame");
      document.documentElement.style.setProperty("--sun", "sun");
      setTimeout("animereset()", 8000);
    },
    resetalarm: function() {
      document.getElementById("houralarm").value = "";
      document.getElementById("minalarm").value = "";
      document.getElementById("houralarm1").value = "";
      document.getElementById("minalarm1").value = "";
    },
    darktheme: function() {
      document.documentElement.style.setProperty('--dial', "black");
      document.documentElement.style.setProperty('--dial-2', '#1b1b1b');
      document.documentElement.style.setProperty('--number-main', "#afeeee");
      document.documentElement.style.setProperty('--number-2', "#afeeee");
      document.documentElement.style.setProperty('--dial-2-scale', "#afeeee");
      document.documentElement.style.setProperty('--scale_ex', "#00bfff");
      document.documentElement.style.setProperty('--second_hand', "#333");
      document.documentElement.style.setProperty('--second_hand_fill', "red");
      document.documentElement.style.setProperty('--minute_hand', "#afeeee");
      document.documentElement.style.setProperty('--minute_hand_fill', "#1f1f1f");
      document.documentElement.style.setProperty('--hour_hand', "#afeeee");
      document.documentElement.style.setProperty('--hour_hand_fill', "#1f1f1f");
    },
    bluetheme: function() {
      document.documentElement.style.setProperty('--dial', "url(#b)");
      document.documentElement.style.setProperty('--dial-2', '#000024');
      document.documentElement.style.setProperty('--number-main', "#a8fcfc");
      document.documentElement.style.setProperty('--number-2', "#a8fcfc");
      document.documentElement.style.setProperty('--dial-2-scale', "#a8fcfc");
      document.documentElement.style.setProperty('--scale_ex', "#a8fcfc");
      document.documentElement.style.setProperty('--grad', "block");
      document.documentElement.style.setProperty('--second_hand', "none");
      document.documentElement.style.setProperty('--second_hand_fill', "red");
      document.documentElement.style.setProperty('--minute_hand', "#a8fcfc");
      document.documentElement.style.setProperty('--minute_hand_fill', "#000060");
      document.documentElement.style.setProperty('--hour_hand', "#a8fcfc");
      document.documentElement.style.setProperty('--hour_hand_fill', "#000060");
    },
    defaulttheme: function() {
      document.documentElement.style.setProperty('--dial', "#0d0d0d");
      document.documentElement.style.setProperty('--dial-2', '#1f1f1f');
      document.documentElement.style.setProperty('--number-main', "cyan");
      document.documentElement.style.setProperty('--number-2', "steelblue");
      document.documentElement.style.setProperty('--dial-2-scale', "steelblue");
      document.documentElement.style.setProperty('--scale_ex', "royalblue");
      document.documentElement.style.setProperty('--second_hand', "none");
      document.documentElement.style.setProperty('--second_hand_fill', "dodgerblue");
      document.documentElement.style.setProperty('--minute_hand', "royalblue");
      document.documentElement.style.setProperty('--minute_hand_fill', "#0d0d0d");
      document.documentElement.style.setProperty('--hour_hand', "royalblue");
      document.documentElement.style.setProperty('--hour_hand_fill', "#0d0d0d");
    },
    daytheme: function() {
      document.documentElement.style.setProperty('--dial-biplan', "#1a1a1a");
      document.documentElement.style.setProperty('--marker-main-biplan', '#ff6600');
      document.documentElement.style.setProperty('--marker-main-fill', "#fcecdf");
      document.documentElement.style.setProperty('--seconds-marker', "#efefef");
    },
    nighttheme: function() {
      document.documentElement.style.setProperty('--dial-biplan', "black");
      document.documentElement.style.setProperty('--marker-main-biplan', '#66ff00');
      document.documentElement.style.setProperty('--marker-main-fill', "#66ff00");
      document.documentElement.style.setProperty('--seconds-marker', "aqua");
    },
    biplanfly: function() {
      this.beginstart = "10s";
    },

    // setalarm: function() {
    // }
  },
  watch: {
    m: function() {
      if (this.m===parseInt(document.getElementById("minalarm").value) && this.h===parseInt(document.getElementById("houralarm").value)) {
        var audio = new Audio(); // Создаём новый элемент Audio
        audio.src = 'mayak.mp3'; // Указываем путь к звуку "клика"
        audio.autoplay = true; // Автоматически запускаем
      }
      else if (this.m===parseInt(document.getElementById("minalarm1").value) && this.h===parseInt(document.getElementById("houralarm1").value)) {
        var audio = new Audio(); // Создаём новый элемент Audio
        audio.src = 'mayak.mp3'; // Указываем путь к звуку "клика"
        audio.autoplay = true; // Автоматически запускаем
      }
    },
  }

})
